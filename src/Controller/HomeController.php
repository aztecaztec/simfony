<?php

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;

//Контроллер главной страницы
class HomeController extends BaseController
{
    /**
    * @Route ("/", name="home")
     */
    public function index()
    {
        $forRender['title']='Добро пожаловать';
        return $this->render('mainpages/index.html.twig',$forRender);
    }
}