<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

//Простой базовый контроллер для проверки наследования
class BaseController extends AbstractController
{
    public function  renderDefault()
    {
        return[
            'title'=>'Значение по умолчанию'
        ];
    }
}