<?php

namespace App\Controller;

use App\Entity\Payments;
use App\Entity\Services;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


//Контроллер страницы отчета
class ReportController extends BaseController
{
    //Отображение страницы
    /**
    * @Route ("/report",name="report")
     */
    public function ReportView()
    {
        $forRender['title']='гг';
        return $this->render('mainpages/report.html.twig',$forRender);
    }

    //Получение отчета
    /**
    * @Route ("/report/get", name="getReport")
     */
    public function GetReports(ManagerRegistry $doctrine,Request $request):Response
    {
        $month=$request->get('month',"none");
        $year=$request->get('year',"none");
        $clientType=$request->get('clientType',"none");
        $type=$request->get('type',"none");
        if ($month == "none" || $year == "none")
        {
            $page=$this->render('elements/report_element.html.twig');
            return new Response($page);
        }
        else
        {
            $date=$year."-".$month;
            if ($type != "none")
            {
                $reports=$doctrine->getRepository(Payments::class)->getReport($date,$clientType);
            }
            else
            {
                $clientType = NULL;
                $reports=$doctrine->getRepository(Payments::class)->getReport($date,$clientType);
            }
            $forRender['arr']=$reports;
            $page=$this->render('elements/report_element.html.twig',$forRender);
            return new Response($page);
        }
    }
}