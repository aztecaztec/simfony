<?php

namespace App\Repository;

use App\Entity\Clients;
use App\Entity\Payments;
use App\Entity\Services;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Payments|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payments|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payments[]    findAll()
 * @method Payments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payments::class);
    }

    // /**
    //  * @return Payments[] Returns an array of Payments objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */






    public function getReport($date,$clientType): array
    {
        //Конвертирование даты в подходящий вид
        $dob=$date.'-01 00:00:00';
        $dobReconverted = \DateTime::createFromFormat('Y-m-d h:i:s', $dob);

        //Составление итогового запроса
        $qb = $this->createQueryBuilder('p');
        $qb->select('s.Name')
            //Получение баланса
            ->addSelect("SUM(CASE WHEN p.Data < :date THEN p.Summa ELSE :null END) balance")
            //Получение прихода
            ->addSelect("SUM(CASE WHEN p.Data >= :date AND (p.Pay_Id = 1 OR p.Pay_Id = 4) THEN p.Summa ELSE :null END) income")
            //Получение расхода
            ->addSelect("SUM(CASE WHEN p.Data >= :date  AND p.Pay_Id = 2 THEN p.Summa ELSE :null END) outcome")
            //Получение перерасчета
            ->addSelect("SUM(CASE WHEN p.Data >= :date AND p.Pay_Id = 3 THEN p.Summa ELSE :null END) recalculation")
            //Получение итогового баланса
            ->addSelect("SUM(p.Summa) total")

            ->leftJoin(Services::class,'s','with','p.Acnt_Id = s.id')
            ->leftJoin(Clients::class,'c','with','p.Client_Id = c.id')
            ->where("p.Data < DATE_ADD(:date,1,'MONTH')")
            ->andWhere("c.Type = CASE WHEN :clientType is NOT NULL THEN :clientType ELSE c.Type END")
            ->groupBy('p.Acnt_Id')

            ->setParameters(array(
                'date' => $dobReconverted,
                'clientType' => $clientType,
                'null' =>NULL,
            ))
        ;

        //Возврат запроса
        $query = $qb->getQuery();
        return $query->getResult();
    }
    /*public function getReportWCT($date,$clientType): array
    {
        //Конвертирование даты в подходящий вид
        $dob=$date.'-01 00:00:00';
        $dobReconverted = \DateTime::createFromFormat('Y-m-d h:i:s', $dob);

        //Составление итогового запроса
        $qb = $this->createQueryBuilder('p');
        $qb->select('s.Name')
            //Получение баланса
            ->addSelect("SUM(CASE WHEN p.Data < :date THEN p.Summa ELSE :null END) balance")
            //Получение прихода
            ->addSelect("SUM(CASE WHEN p.Data >= :date AND (p.Pay_Id = 1 OR p.Pay_Id = 4) THEN p.Summa ELSE :null END) income")
            //Получение расхода
            ->addSelect("SUM(CASE WHEN p.Data >= :date  AND p.Pay_Id = 2 THEN p.Summa ELSE :null END) outcome")
            //Получение перерасчета
            ->addSelect("SUM(CASE WHEN p.Data >= :date AND p.Pay_Id = 3 THEN p.Summa ELSE :null END) recalculation")
            //Получение итогового баланса
            ->addSelect("SUM(p.Summa) total")
            ->leftJoin(Services::class,'s','with','p.Acnt_Id = s.id')
            ->leftJoin(Clients::class,'c','with','p.Client_Id = c.id')
            ->where("p.Data < DATE_ADD(:date,1,'MONTH')")
            ->andWhere("c.Type = :clientType")
            ->groupBy('p.Acnt_Id')
            ->setParameters(array(
                'date' => $dobReconverted,
                'clientType' => $clientType,
                'null' =>NULL,
            ))
        ;

        //Возврат запроса
        $query = $qb->getQuery();
        return $query->getResult();
    }*/

    /*
    public function findOneBySomeField($value): ?Payments
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
