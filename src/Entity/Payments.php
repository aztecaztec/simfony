<?php

namespace App\Entity;

use App\Repository\PaymentsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PaymentsRepository::class)]
class Payments
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Clients::class, inversedBy: 'payments', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private $Client_Id;

    #[ORM\Column(type: 'float')]
    private $Summa;

    #[ORM\Column(type: 'date')]
    private $Data;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Description;

    #[ORM\ManyToOne(targetEntity: Services::class, inversedBy: 'payments')]
    #[ORM\JoinColumn(nullable: false)]
    private $Acnt_Id;

    #[ORM\ManyToOne(targetEntity: PaymentType::class, inversedBy: 'payments')]
    #[ORM\JoinColumn(nullable: false)]
    private $Pay_Id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientId(): ?Clients
    {
        return $this->Client_Id;
    }

    public function setClientId(?Clients $Client_Id): self
    {
        $this->Client_Id = $Client_Id;

        return $this;
    }

    public function getSumma(): ?float
    {
        return $this->Summa;
    }

    public function setSumma(float $Summa): self
    {
        $this->Summa = $Summa;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->Data;
    }

    public function setData(\DateTimeInterface $Data): self
    {
        $this->Data = $Data;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getAcntId(): ?Services
    {
        return $this->Acnt_Id;
    }

    public function setAcntId(?Services $Acnt_Id): self
    {
        $this->Acnt_Id = $Acnt_Id;

        return $this;
    }

    public function getPayId(): ?PaymentType
    {
        return $this->Pay_Id;
    }

    public function setPayId(?PaymentType $Pay_Id): self
    {
        $this->Pay_Id = $Pay_Id;

        return $this;
    }
}
