<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220118080103 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, type INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_type (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payments (id INT AUTO_INCREMENT NOT NULL, client_id_id INT NOT NULL, acnt_id_id INT NOT NULL, pay_id_id INT NOT NULL, summa DOUBLE PRECISION NOT NULL, data DATE NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_65D29B32DC2902E0 (client_id_id), INDEX IDX_65D29B3221B92F80 (acnt_id_id), INDEX IDX_65D29B3237A531CE (pay_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE services (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B32DC2902E0 FOREIGN KEY (client_id_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B3221B92F80 FOREIGN KEY (acnt_id_id) REFERENCES services (id)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B3237A531CE FOREIGN KEY (pay_id_id) REFERENCES payment_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B32DC2902E0');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B3237A531CE');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B3221B92F80');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE payment_type');
        $this->addSql('DROP TABLE payments');
        $this->addSql('DROP TABLE services');
    }
}
